from bs4 import BeautifulSoup
import requests
import csv
import argparse

parser = argparse.ArgumentParser(description='A scrapper to get all Amelie listings for specified professionals in certain area.')
parser.add_argument("-pn", help = "Specify the profession number you are looking for.")
parser.add_argument("-r", help = "Specify the region (number is sufficient) you are in.")
args = parser.parse_args()

class Professionnal:
    def __init__(self, name, phoneNumber, street) :
        self.name = name
        self.phoneNumber = phoneNumber
        self.street = street

url = 'http://annuairesante.ameli.fr/recherche.html'
formData = {
    'ps_localisation': args.r, 
    'ps_profession': args.pn,
    'ps_proximite': 'on',
    'localisation_category': 'departements',
    'type': 'ps'
}

myRequest = requests.post('http://annuairesante.ameli.fr/recherche.html', formData)
soup = BeautifulSoup(myRequest.text, 'html.parser')
scrappedProList = soup.find_all('div', {"class": 'item-professionnel'})

proList = []

page = 1
while page <= 10:
    print(page)
    page = page + 1

for scrappedPro in scrappedProList:
    name = scrappedPro.find('div', {"class": "nom_pictos"}).text
    phoneNumber = ''
    scrappedPhoneNumber = scrappedPro.find('div', {"class": "tel"})
    if(scrappedPhoneNumber): 
        phoneNumber = scrappedPhoneNumber.text
    street = scrappedPro.find('div', {"class": "adresse"})
    for br in street('br'):
        br.replace_with(' ')
    proList.append(
        Professionnal(
            name = name, 
            phoneNumber = phoneNumber,
            street = street.text
        )
    )

with open('professionals.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerow(["Name", "Phone number", "Address"])
    for pro in proList:
        writer.writerow([pro.name, pro.phoneNumber, pro.street])